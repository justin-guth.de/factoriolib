from factoriolib._decorators import name

class Item:

    @staticmethod
    def name() -> str: return "item"

@name("accumulator")
class Accumulator(Item): pass

@name("advanced-circuit")
class AdvancedCircuit(Item): pass

@name("arithmetic-combinator")
class ArithmeticCombinator(Item): pass

@name("artillery-turret")
class ArtilleryTurret(Item): pass

@name("assembling-machine-1")
class AssemblingMachine1(Item): pass

@name("assembling-machine-2")
class AssemblingMachine2(Item): pass

@name("assembling-machine-3")
class AssemblingMachine3(Item): pass

@name("battery")
class Battery(Item): pass

@name("battery-equipment")
class BatteryEquipment(Item): pass

@name("battery-mk2-equipment")
class BatteryMk2Equipment(Item): pass

@name("beacon")
class Beacon(Item): pass

@name("belt-immunity-equipment")
class BeltImmunityEquipment(Item): pass

@name("big-electric-pole")
class BigElectricPole(Item): pass

@name("boiler")
class Boiler(Item): pass

@name("burner-generator")
class BurnerGenerator(Item): pass

@name("burner-inserter")
class BurnerInserter(Item): pass

@name("burner-mining-drill")
class BurnerMiningDrill(Item): pass

@name("centrifuge")
class Centrifuge(Item): pass

@name("chemical-plant")
class ChemicalPlant(Item): pass

@name("coal")
class Coal(Item): pass

@name("coin")
class Coin(Item): pass

@name("concrete")
class Concrete(Item): pass

@name("constant-combinator")
class ConstantCombinator(Item): pass

@name("construction-robot")
class ConstructionRobot(Item): pass

@name("copper-cable")
class CopperCable(Item): pass

@name("copper-ore")
class CopperOre(Item): pass

@name("copper-plate")
class CopperPlate(Item): pass

@name("crude-oil-barrel")
class CrudeOilBarrel(Item): pass

@name("decider-combinator")
class DeciderCombinator(Item): pass

@name("discharge-defense-equipment")
class DischargeDefenseEquipment(Item): pass

@name("electric-energy-interface")
class ElectricEnergyInterface(Item): pass

@name("electric-engine-unit")
class ElectricEngineUnit(Item): pass

@name("electric-furnace")
class ElectricFurnace(Item): pass

@name("electric-mining-drill")
class ElectricMiningDrill(Item): pass

@name("electronic-circuit")
class ElectronicCircuit(Item): pass

@name("empty-barrel")
class EmptyBarrel(Item): pass

@name("energy-shield-equipment")
class EnergyShieldEquipment(Item): pass

@name("energy-shield-mk2-equipment")
class EnergyShieldMk2Equipment(Item): pass

@name("engine-unit")
class EngineUnit(Item): pass

@name("exoskeleton-equipment")
class ExoskeletonEquipment(Item): pass

@name("explosives")
class Explosives(Item): pass

@name("express-loader")
class ExpressLoader(Item): pass

@name("express-splitter")
class ExpressSplitter(Item): pass

@name("express-transport-belt")
class ExpressTransportBelt(Item): pass

@name("express-underground-belt")
class ExpressUndergroundBelt(Item): pass

@name("fast-inserter")
class FastInserter(Item): pass

@name("fast-loader")
class FastLoader(Item): pass

@name("fast-splitter")
class FastSplitter(Item): pass

@name("fast-transport-belt")
class FastTransportBelt(Item): pass

@name("fast-underground-belt")
class FastUndergroundBelt(Item): pass

@name("filter-inserter")
class FilterInserter(Item): pass

@name("flamethrower-turret")
class FlamethrowerTurret(Item): pass

@name("flying-robot-frame")
class FlyingRobotFrame(Item): pass

@name("fusion-reactor-equipment")
class FusionReactorEquipment(Item): pass

@name("gate")
class Gate(Item): pass

@name("green-wire")
class GreenWire(Item): pass

@name("gun-turret")
class GunTurret(Item): pass

@name("hazard-concrete")
class HazardConcrete(Item): pass

@name("heat-exchanger")
class HeatExchanger(Item): pass

@name("heat-interface")
class HeatInterface(Item): pass

@name("heat-pipe")
class HeatPipe(Item): pass

@name("heavy-oil-barrel")
class HeavyOilBarrel(Item): pass

@name("infinity-chest")
class InfinityChest(Item): pass

@name("infinity-pipe")
class InfinityPipe(Item): pass

@name("inserter")
class Inserter(Item): pass

@name("iron-chest")
class IronChest(Item): pass

@name("iron-gear-wheel")
class IronGearWheel(Item): pass

@name("iron-ore")
class IronOre(Item): pass

@name("iron-plate")
class IronPlate(Item): pass

@name("iron-stick")
class IronStick(Item): pass

@name("item-unknown")
class ItemUnknown(Item): pass

@name("lab")
class Lab(Item): pass

@name("land-mine")
class LandMine(Item): pass

@name("landfill")
class Landfill(Item): pass

@name("laser-turret")
class LaserTurret(Item): pass

@name("light-oil-barrel")
class LightOilBarrel(Item): pass

@name("linked-belt")
class LinkedBelt(Item): pass

@name("linked-chest")
class LinkedChest(Item): pass

@name("loader")
class Loader(Item): pass

@name("logistic-chest-active-provider")
class LogisticChestActiveProvider(Item): pass

@name("logistic-chest-buffer")
class LogisticChestBuffer(Item): pass

@name("logistic-chest-passive-provider")
class LogisticChestPassiveProvider(Item): pass

@name("logistic-chest-requester")
class LogisticChestRequester(Item): pass

@name("logistic-chest-storage")
class LogisticChestStorage(Item): pass

@name("logistic-robot")
class LogisticRobot(Item): pass

@name("long-handed-inserter")
class LongHandedInserter(Item): pass

@name("low-density-structure")
class LowDensityStructure(Item): pass

@name("lubricant-barrel")
class LubricantBarrel(Item): pass

@name("medium-electric-pole")
class MediumElectricPole(Item): pass

@name("night-vision-equipment")
class NightVisionEquipment(Item): pass

@name("nuclear-fuel")
class NuclearFuel(Item): pass

@name("nuclear-reactor")
class NuclearReactor(Item): pass

@name("offshore-pump")
class OffshorePump(Item): pass

@name("oil-refinery")
class OilRefinery(Item): pass

@name("personal-laser-defense-equipment")
class PersonalLaserDefenseEquipment(Item): pass

@name("personal-roboport-equipment")
class PersonalRoboportEquipment(Item): pass

@name("personal-roboport-mk2-equipment")
class PersonalRoboportMk2Equipment(Item): pass

@name("petroleum-gas-barrel")
class PetroleumGasBarrel(Item): pass

@name("pipe")
class Pipe(Item): pass

@name("pipe-to-ground")
class PipeToGround(Item): pass

@name("plastic-bar")
class PlasticBar(Item): pass

@name("player-port")
class PlayerPort(Item): pass

@name("power-switch")
class PowerSwitch(Item): pass

@name("processing-unit")
class ProcessingUnit(Item): pass

@name("programmable-speaker")
class ProgrammableSpeaker(Item): pass

@name("pump")
class Pump(Item): pass

@name("pumpjack")
class Pumpjack(Item): pass

@name("radar")
class Radar(Item): pass

@name("rail-chain-signal")
class RailChainSignal(Item): pass

@name("rail-signal")
class RailSignal(Item): pass

@name("red-wire")
class RedWire(Item): pass

@name("refined-concrete")
class RefinedConcrete(Item): pass

@name("refined-hazard-concrete")
class RefinedHazardConcrete(Item): pass

@name("roboport")
class Roboport(Item): pass

@name("rocket-control-unit")
class RocketControlUnit(Item): pass

@name("rocket-fuel")
class RocketFuel(Item): pass

@name("rocket-part")
class RocketPart(Item): pass

@name("rocket-silo")
class RocketSilo(Item): pass

@name("satellite")
class Satellite(Item): pass

@name("simple-entity-with-force")
class SimpleEntityWithForce(Item): pass

@name("simple-entity-with-owner")
class SimpleEntityWithOwner(Item): pass

@name("small-electric-pole")
class SmallElectricPole(Item): pass

@name("small-lamp")
class SmallLamp(Item): pass

@name("solar-panel")
class SolarPanel(Item): pass

@name("solar-panel-equipment")
class SolarPanelEquipment(Item): pass

@name("solid-fuel")
class SolidFuel(Item): pass

@name("splitter")
class Splitter(Item): pass

@name("stack-filter-inserter")
class StackFilterInserter(Item): pass

@name("stack-inserter")
class StackInserter(Item): pass

@name("steam-engine")
class SteamEngine(Item): pass

@name("steam-turbine")
class SteamTurbine(Item): pass

@name("steel-chest")
class SteelChest(Item): pass

@name("steel-furnace")
class SteelFurnace(Item): pass

@name("steel-plate")
class SteelPlate(Item): pass

@name("stone")
class Stone(Item): pass

@name("stone-brick")
class StoneBrick(Item): pass

@name("stone-furnace")
class StoneFurnace(Item): pass

@name("stone-wall")
class StoneWall(Item): pass

@name("storage-tank")
class StorageTank(Item): pass

@name("substation")
class Substation(Item): pass

@name("sulfur")
class Sulfur(Item): pass

@name("sulfuric-acid-barrel")
class SulfuricAcidBarrel(Item): pass

@name("train-stop")
class TrainStop(Item): pass

@name("transport-belt")
class TransportBelt(Item): pass

@name("underground-belt")
class UndergroundBelt(Item): pass

@name("uranium-235")
class Uranium235(Item): pass

@name("uranium-238")
class Uranium238(Item): pass

@name("uranium-fuel-cell")
class UraniumFuelCell(Item): pass

@name("uranium-ore")
class UraniumOre(Item): pass

@name("used-up-uranium-fuel-cell")
class UsedUpUraniumFuelCell(Item): pass

@name("water-barrel")
class WaterBarrel(Item): pass

@name("wood")
class Wood(Item): pass

@name("wooden-chest")
class WoodenChest(Item): pass